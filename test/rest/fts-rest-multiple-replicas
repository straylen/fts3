#!/usr/bin/env python
# meta: proxy=true
# meta: preconfig=../FTS3-config
import itertools
import logging
from lib import base, storage 


class TestMultipleReplicas(base.TestRepeatEach):

    def setUp(self):
        # Need to create three sources!
        self.transfers = []
        for (srcSa, dstSa) in storage.getStoragePairs():
            srcSurl1 = self.surl.generate(srcSa)
            srcSurl2 = self.surl.generate(srcSa)
            srcSurl3 = self.surl.generate(srcSa)
            dstSurl = self.surl.generate(dstSa)
            self.transfers.append((srcSurl1, srcSurl2, srcSurl3, dstSurl))

    def iterations(self):
        for t in self.transfers:
            t_repr = "%s => %s" % (t[0].split(':')[0], t[-1].split(':')[0])
            yield (t_repr, t)

    def test_multipleWithReuse(self, transfer):
        """
        Submitting multiple replicas with reuse enabled must fail,
        since those are two incompatible modes
        """
        (src1, src2, src3, dst) = transfer
        logging.info("Transfer with multiple sources and reuse enabled")
        logging.info(src1)
        logging.info(src2)
        logging.info(src3)
        logging.info("\t=> %s" % dst)
        self.assertRaises(Exception, self.client.submit,
            [{'sources': [src1, src2, src3],'destinations': [dst]}],
            ['-r'])


    def test_multipleNoExists(self, transfer):
        """
        Three source replicas are given, but none of them exists,
        so the job final status must be failed
        """
        (src1, src2, src3, dst) = transfer
        logging.info("Transfer with multiple nonexistent sources")
        logging.info(src1)
        logging.info(src2)
        logging.info(src3)
        logging.info("\t=> %s" % dst)
        jobId = self.client.submit([{'sources': [src1, src2, src3],'destinations': [dst]}])
        logging.info("Got job id %s" % jobId)
        state = self.client.poll(jobId)
        logging.info("Job %s finished with %s" % (jobId, state))
        self.assertEqual('FAILED', state, 'job.state')

        files = self.client.getFileInfo(jobId)
        i = 0
        for pair in files.keys():
            label = 'jobs.files[' + str(i) + '].state'
            self.assertEqual('FAILED', files[pair]['file_state'], label)
            i += 1


    def test_multipleOneExists(self, transfer):
        """
        Three source replicas are given, and only one exists.
        The transfer must succeed. 
        """
        (src1, src2, src3, dst) = transfer
        logging.info("Transfer with multiple sources, only one exists")
        logging.info(src1)
        logging.info(src2)
        logging.info(src3)
        self.surl.create(src3)
        logging.info("\t=> %s" % dst)
        jobId = self.client.submit([{'sources': [src1, src2, src3],'destinations': [dst]}])
        logging.info("Got job id %s" % jobId)
        state = self.client.poll(jobId)
        logging.info("Job %s finished with %s" % (jobId, state))
        self.assertEqual('FINISHED', state, 'job.state')

        files = self.client.getFileInfo(jobId)
        i = 0
        for (src, dst) in files.keys():
            label = 'jobs.files[' + str(i) + '].state'
            if src == src3:
                self.assertEqual('FINISHED', files[(src, dst)]['file_state'], label)
            else:
                self.assertIn(['FAILED', 'NOT_USED'], files[(src, dst)]['file_state'], label)
            i += 1
    

if __name__ == '__main__':
    import sys
    sys.exit(TestMultipleReplicas().run())

