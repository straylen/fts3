#!/usr/bin/env python
# meta: proxy=true
# meta: preconfig=../FTS3-config
import itertools
import logging
from lib import base, config


class TestUserChecksum(base.TestRepeatEach):

    def test_withUserChecksum(self, transfer):
        """
        Ask to validate the checksum and provide the right checksum
        """
        src, dst = transfer
        logging.info("Transfer with user checksum %s => %s" % (src, dst))
        srcChecksum = self.surl.create(src)
        jobId = self.client.submit([{'sources': [src], 'destinations': [dst], 'checksums': [srcChecksum]}], [srcChecksum])
        logging.info("Got job id %s" % jobId)
        state = self.client.poll(jobId)
        logging.info("Finished with %s" % state)
        self.assertEqual('FINISHED', state, 'job.state')
        dstChecksum = self.surl.checksum(dst)
        self.assertEqual(srcChecksum, dstChecksum, 'file.checksum')

    def _badChecksum(self, chk):
        """
        To generate a bad checksum, we get the right one
        and add any value.
        """
        (al, val) = chk.split(':')
        val = "%x" % (int(val, 16) + 42)
        return al + ':' + val

    def test_withBadUserChecksum(self, transfer):
        """
        Ask to validate the checksum but give a bad one on purpose
        """
        src, dst = transfer
        logging.info("Transfer with wrong user checksum %s => %s" % (src, dst))
        srcChecksum = self.surl.create(src)
        badChecksum = self._badChecksum(srcChecksum)
        jobId = self.client.submit([{'sources': [src], 'destinations': [dst], 'checksum': badChecksum}], ['-K', badChecksum , '-o', srcChecksum ])
        logging.info("Got job id %s" % jobId)
        state = self.client.poll(jobId)
        logging.info("Finished with %s" % state)
        self.assertEqual('FAILED', state, 'job.state')

    def test_withBadUserChecksumNoFlag(self, transfer):   
        """
        Same as before, but -K is not passed. The transfer must fail!
        """
        src, dst = transfer
        logging.info("Transfer with wrong user checksum and no flag %s => %s" % (src, dst))
        srcChecksum = self.surl.create(src)
        badChecksum = self._badChecksum(srcChecksum)
        jobId = self.client.submit([{'sources': [src], 'destinations': [dst], 'checksum': badChecksum}], [srcChecksum])
        logging.info("Got job id %s" % jobId)
        state = self.client.poll(jobId)
        logging.info("Finished with %s" % state)
        self.assertEqual('FAILED', state, 'job.state')


if __name__ == '__main__':
    import sys
    sys.exit(TestUserChecksum().run())
